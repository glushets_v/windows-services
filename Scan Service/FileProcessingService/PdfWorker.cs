﻿using System.IO;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.Rendering;

namespace FileProcessingService
{
    public class PdfWorker
    {
        public void CreatePdf(string inDir, string outFileName)
        {
            var document = new Document();
            var section = document.AddSection();

            foreach (var file in Directory.EnumerateFiles(inDir))
            {
                var img = section.AddImage(file);

                img.RelativeHorizontal = RelativeHorizontal.Page;
                img.RelativeVertical = RelativeVertical.Page;

                img.Top = 0;
                img.Left = 0;

                img.Height = document.DefaultPageSetup.PageHeight;
                img.Width = document.DefaultPageSetup.PageWidth;

                section.AddPageBreak(); // разделитель после каждой картинки
            }

            var render = new PdfDocumentRenderer
            {
                Document = document
            };

            render.RenderDocument();
            render.Save(Path.Combine(inDir, outFileName));
        }
    }
}
