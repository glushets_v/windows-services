﻿using System.Diagnostics;
using System.IO;
using NLog;
using NLog.Config;
using NLog.Targets;
using Topshelf;

namespace FileProcessingService
{
    class Program
    {
        static void Main(string[] args)
        {
            var currentDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);

            var inDir = Path.Combine(currentDir, "in");
            var outDir = Path.Combine(currentDir, "out");

            var logConf = new LoggingConfiguration();
            var target = new FileTarget
            {
                Name = "Def",
                FileName = Path.Combine(currentDir, "log.txt"),
                Layout = "${date} ${message} ${onexception:inner=${exception:format=toString}}"
            };

            logConf.AddTarget(target);
            logConf.AddRuleForAllLevels(target);

            var logFactiry = new LogFactory(logConf);

            HostFactory.Run(conf => conf.Service<FileCopyService>(
                    serv =>
                    {
                        serv.ConstructUsing(() => new FileCopyService(inDir, outDir));      // как проинициализировать сервис
                        serv.WhenStarted(s => s.Start());
                        serv.WhenStopped(s => s.Stop());
                    }
                ).UseNLog(logFactiry)
            );
        }
    }
}
