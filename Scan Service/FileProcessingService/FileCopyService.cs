﻿using System;
using System.IO;
using System.Linq;
using System.Threading;

namespace FileProcessingService
{
    public class FileCopyService
    {
        private FileSystemWatcher watcher;

        private string inDir;
        private string outDir;

        private Thread workingThread;
        private ManualResetEvent workStop;
        private AutoResetEvent newFile;

        private PdfWorker documentWorker;
        private readonly string[] validExtensions = {"jpg", "bmp", "gif", "png", "jpeg"};

        private int folderNum = 1;
        private string outFileName = "out.pdf";

        private DateTime lastFileDate;
        private const int TimeOut = 3;

        public FileCopyService(string inDir, string outDir)
        {
            this.inDir = inDir;
            this.outDir = outDir;

            if (!Directory.Exists(inDir))
            {
                Directory.CreateDirectory(inDir);
            }

            if (!Directory.Exists(outDir))
            {
                Directory.CreateDirectory(outDir);
            }

            workingThread = new Thread(WorkProc);
            workStop = new ManualResetEvent(false);
            newFile = new AutoResetEvent(false);

            watcher = new FileSystemWatcher(inDir);
            watcher.Created += FileCreate;

            documentWorker = new PdfWorker();

            lastFileDate = DateTime.Now;
        }

        private void WorkProc()
        {
            string moveDirName = string.Concat(outDir, folderNum);
            var imageNumBefore = 0;

            do
            {
                var files = Directory.EnumerateFiles(inDir).ToArray();

                for (int i = 0; i < files.Count(); i++)
                {
                    var fileName = Path.GetFileName(files[i]);
                    var splitedFileName = SplitFileName(fileName);

                    if (CheckImageFormate(splitedFileName.Last().ToLower()))
                    {
                        int num;
                        if (int.TryParse(splitedFileName[1], out num))
                        {
                            if (num != imageNumBefore + 1 || !CheckTimout())
                            {
                                documentWorker.CreatePdf(moveDirName, string.Concat(folderNum, "_", outFileName));
                                folderNum++;
                                moveDirName = string.Concat(outDir, folderNum);
                            }

                            if (!MoveFile(files[i], fileName, moveDirName)) // если еще не дали команду на остановку
                            {
                                return;
                            }

                            if (i == files.Count() - 1)
                            {
                                documentWorker.CreatePdf(moveDirName, string.Concat(folderNum, "_", outFileName));
                            }

                            imageNumBefore = num;
                            lastFileDate = DateTime.Now;
                        }
                    }
                }
            } while (WaitHandle.WaitAny(new WaitHandle[] {workStop, newFile}) != 0);
        }

        private bool CheckTimout()
        {
            TimeSpan diff = DateTime.Now.Subtract(lastFileDate);

            return diff.TotalMinutes >= 0 && diff.TotalMinutes <= TimeOut;
        }

        private bool MoveFile(string file, string fileName, string outDirName)
        {
            if (workStop.WaitOne(TimeSpan.Zero)) // если еще не дали команду на остановку
            {
                return false;
            }

            if (TryOpen(file, 3))
            {
                if (!Directory.Exists(outDirName))
                {
                    Directory.CreateDirectory(outDirName);
                }

                File.Move(file, Path.Combine(outDirName, fileName));
            }

            return true;
        }

        private bool CheckImageFormate(string fileExt)
        {
            return validExtensions.Contains(fileExt);
        }

        private string[] SplitFileName(string fileName)
        {
            return fileName.Split('.', '_');
        }

        private void FileCreate(object sender, FileSystemEventArgs e)
        {
            newFile.Set(); // инит события что новый файл появился
        }

        private bool TryOpen(string fullPath, int v)
        {
            for (int j = 0; j < v; j++)
            {
                try
                {
                    var file = File.Open(fullPath, FileMode.Open, FileAccess.Read, FileShare.None); // открываем в монопольном доступе
                    file.Close();

                    return true;
                }
                catch (IOException)
                {
                    Thread.Sleep(3000);
                }
            }
            return false;
        }

        public void Start()
        {
            workingThread.Start();
            watcher.EnableRaisingEvents = true;
        }

        public void Stop()
        {
            watcher.EnableRaisingEvents = false;
            workStop.Set();
            workingThread.Join();
        }
    }
}